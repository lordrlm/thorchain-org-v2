import CoingeckoTable from "../components/page-components/CoingeckoTable.vue"
import Tweet from "../components/page-components/Tweet.vue"
import utils from "../utils"
import TelegramIcon from "../assets/images/icons/social-telegram-icon-hover.svg?inline"
import GithubIcon from "../assets/images/icons/social-github-icon.svg?inline"
import GitlabIcon from "../assets/images/icons/social-gitlab-icon.svg?inline"
import TwitterIcon from "../assets/images/icons/social-twitter-icon.svg?inline"
import CountDownTimer from '../components/page-components/CountDownTimer.vue'
import FeatureCards from '../components/page-components/FeatureCards.vue'
import {fetchHomePageData} from "../utils/commonFunctions"

{/* <script async src="https://telegram.org/js/telegram-widget.js?14" data-telegram-discussion="durov/126" data-comments-limit="5" data-colorful="1" data-color="F3BA2F" data-dark="1"></script> */}
export default{
    head() {
      return {
        
      }
    },
    components: { 
        CoingeckoTable,
        Tweet,
        TelegramIcon,
        GithubIcon,
        GitlabIcon,
        TwitterIcon,
        CountDownTimer,
        FeatureCards
     },
    mounted(){

    },
    async fetch() {
        
        // filling up Tools Table
        var toolsTableContent = this.$t('homePage.toolsTableContent')
        for(var key of Object.keys(toolsTableContent)){
          var record = toolsTableContent[key]
          record['link'] = this.$links(`tools_table.${key}.tool_link`)
          record['repo_url'] = this.$links(`tools_table.${key}.repo_url`)
          record['telegram_url'] = this.$links(`tools_table.${key}.telegram_url`)
          record['github_url'] = this.$links(`tools_table.${key}.github_url`)
          record['gitlab_url'] = this.$links(`tools_table.${key}.gitlab_url`)
          record['twitter_url'] = this.$links(`tools_table.${key}.twitter_url`)
          this.toolsTable.push(record)
        }
        this.toolsTable.sort(function(a, b){
          if(a.category < b.category) { return -1; }
          if(a.category > b.category) { return 1; }
          return 0;
        })
        
    },
    methods:{
      titleButSirMouseoverEvent(event){
        this.titleButSirMouseover = true;
      },
      titleButSirMouseoutEvent(event){
        this.titleButSirMouseover = false;
      },
      titleButSirClicked(event){
        console.log(this.$links('explainer'))
        gtag('event', this.$links('explainer').action, {
          'event_category': this.$links('explainer').category,
          'event_label': this.$links('explainer').label,
          'link': this.$links('explainer').link
        });
        this.$store.commit('showYoutubeOverlay', true)
      }
    },
    async asyncData({$services}){
      var {
        nodeTweetText,
        nodeTweetDate,
        nodesAPY,
        lpAPY,
        activeNodeCount,
        standbyNodeCount,
        totalPooledUsd,
        totalCapitalUsd,
        totalBondedUsd,
        volume24h,
        tx24h,
        users24h,
        runeLocked,
        MCCN
      } = await fetchHomePageData($services)
      return {
        nodeTweetText,
        nodeTweetDate,
        nodesAPY,
        lpAPY,
        activeNodeCount,
        standbyNodeCount,
        totalPooledUsd,
        totalCapitalUsd,
        totalBondedUsd,
        volume24h,
        tx24h,
        users24h,
        runeLocked,
        MCCN
      }        
    },
    data(){
        return {
            titleButSirMouseover: false,
            nodesAPY: 23,
            lpAPY: 123,
            activeNodeCount: 0,
            standbyNodeCount: 0,
            totalCapitalUsd: 0,
            totalBondedUsd: 0,
            totalPooledUsd: 0,
            volume24h: 0,
            tx24h: 0,
            users24h: 0,
            runeLocked: 0,
            whichCard: 0,
            toolsTable:[],
            nodeTweetText: ""
         }
    },
    head() {
        return {

            title: 'THORChain',
            meta: [
                    {property: "og:locale", content: "en_US"},
                    {property: "og:type", content: "website"},
                    {property: "og:title", content: "THORChain"},
                    {property: "og:description", content: "THORChain is a decentralized liquidity network. Deposit native assets into Liquidity Pools to earn yield. The network is 100% autonomous and decentralized."},
                    {property: "og:url", content: process.env.baseUrl},
                    {property: "og:site_name", content: "THORChain"},
                    {property: "og:image", content: process.env.baseUrl + "/homepage-metacard.png"},
                    {property: "og:image:width", content: "876"},
                    {property: "og:image:heigh", content: "438"},
                    {name: "twitter:creator", content: "@thorchain_org"},
                    {name: "twitter:site", content: "@thorchain_org"},
                    {name: "twitter:title", content: "THORChain"},
                    {name: "twitter:description", content: "THORChain is a decentralized liquidity network. Deposit native assets into Liquidity Pools to earn yield. The network is 100% autonomous and decentralized."},
                    {name: "twitter:card", content: "summary_large_image"},
                    {name: "twitter:image", content: process.env.baseUrl + "/homepage-metacard.png"},
            ],
        }
    },
}