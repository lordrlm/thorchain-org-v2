import utils from "../utils"
export default ({ $axios, store, $links}, inject) =>{
    // console.log("$fetchData called")
    inject('services', {
        async getLastBlock(){
            try{
                const url = process.env.baseUrl + "/api/static_data/lastBlock";
                var res = await $axios.get(url)
                var data = res.data
                return data
            } catch(e){
                return null
            }
        },
        async getMinimumBond(){
            try{
                var url = process.env.baseUrl + '/api/static_data/minBond';
                var res = await $axios.get(url)
                var minimumBond = res.data
                minimumBond = minimumBond / 100000000; // in Rune
                minimumBond = parseInt(minimumBond)
                return minimumBond
            }catch(e){
                return null
            }
        },
        async getRunePrice(){
            try{
                var url = process.env.baseUrl + '/api/static_data/runePrice';
                var res = await $axios.get(url)
                return res.data
            } catch(e){
                return null
            }
        },
        async getActiveAsgardVaultLink(){
            console.log("getActiveAsgardVaultLink")
            try{

                var url = process.env.baseUrl + '/api/static_data/activeAsgardVaultLink'
                var res = await $axios.get(url)
                var link =  res.data
                var linkProps = $links('viewblock_vault_asgard')
                linkProps.link = link + ""
                return linkProps
            } catch(e){
                console.log("Error: ")
                console.log(e)
                return null
            }
        },
        async getThorTotalTransactions(){
            try{
                var url = process.env.baseUrl + '/api/static_data/thorTotalTx'
                var res = await $axios.get(ur)
                return res.data
            }
            catch(e){
                return null
            }
        },
        async getHistoricalVolume(from, to){
            console.log("get historical volume")
            var data = await fetch(
                `https://chaosnet-midgard.bepswap.com/v1/history/total_volume?interval=day&from=${from}&to=${to}`
              ).then((res) => res.json());
              var usdData = await fetch(
                `https://chaosnet-midgard.bepswap.com/v1/history/pools?pool=BNB.BUSD-BD1&interval=day&from=${from}&to=${to}`
              ).then((res) => res.json());
            return {data, usdData}
        },
        async getCoingeckoData(){
            try{
                const url = process.env.baseUrl + '/api/coingecko'
                var res = await $axios.get(url)
                if(res.data){
                    var data = res.data
                    data['valid'] = true
                }
                else
                var data = {data:null, updatedAt:null, valid: false}
                return data
            }catch(e){
                return null
            }
        },
        async getThorNetValues(){
            try {
                var res = await $axios.get(process.env.baseUrl + '/api/static_data/thorNetValues')
                var netValues = res.data
                return netValues
              } catch (e) {
                console.log("fetching ThorNetValues failed")
                console.log(e)
                return null
              }
        },
        async getThorNetValuesMCCN(){
            try {
                var res = await $axios.get(process.env.baseUrl + '/api/static_data/thorNetValuesMCCN')
                var netValues = res.data
                return netValues
              } catch (e) {
                console.log("fetching ThorNetValuesMCCN failed")
                console.log(e)
                return null
              }
        },
        async getPoolsTableData(){
            var res = await $axios.$get(process.env.baseUrl + '/api/static_data/poolsTableData')
            return res
        },
        async getERC20Markets(){
            try{
                var res = await $axios.get(process.env.baseUrl + '/api/static_data/congecko_ERC20_Markets')
                return res.data
            }
            catch(e){
                console.log("fetching erc20 markets failed")
                console.log(e)
                return []
            }
        },
        async getRuneLocked(){
            var netValues = await this.getThorNetValues()
            var coingecko = await this.getCoingeckoData()
            if(netValues && netValues.totalCapital && coingecko && coingecko.netInfo){
                var totalCapital = parseInt(netValues.totalCapital / 100000000)
                var circulatingSupply = parseInt(coingecko.netInfo.circulatingSupply)
                var runeLocked = (totalCapital / circulatingSupply) * 100
                runeLocked = parseInt(runeLocked)
                return runeLocked
            }
            return null
        },
        async getAPY(){
            var netValues = await this.getThorNetValues()
            if(netValues){
                var nodesAPY = parseInt(netValues.bondingAPY * 100)
                var lpAPY = parseInt(netValues.liquidityAPY * 100)
                return {nodesAPY, lpAPY}
            }
            return {nodesAPY: null, lpAPY: null}
        },
        async getNetworkCapital(){
            var netValues = await this.getThorNetValues()
            var runePrice = await this.getRunePrice()

            if(netValues && runePrice){
                var totalStaked = netValues.totalStaked / 100000000
                totalStaked = parseInt(totalStaked * runePrice) * 2
                totalStaked = utils.numberWithCommas(totalStaked)
                var totalPooledUsd = totalStaked
                
                var totalCapital = parseInt(netValues.totalCapital / 100000000)
                var totalCapitalUSD = parseInt(totalCapital * runePrice)
                var totalCapitalTxt = utils.numberWithCommas(totalCapitalUSD)
                var totalCapitalUsd = totalCapitalTxt
                
                var totalBonded = parseInt(netValues.totalActiveBond / 100000000)
                totalBonded = parseInt(totalBonded * runePrice)
                var totalBondedUsd = utils.numberWithCommas(totalBonded)

                return {totalPooledUsd, totalBondedUsd, totalCapitalUsd}
            }
            return {totalPooledUsd: null, totalBondedUsd: null, totalCapitalUsd: null}
        },
        async getNodesActiveAndStandbyCount(){
            var netValues = await this.getThorNetValues()
            if(netValues){
                var activeNodeCount = netValues.activeNodeCount
                var standbyNodeCount = netValues.standbyNodeCount
                return {activeNodeCount, standbyNodeCount}
            }
            return {activeNodeCount: null, standbyNodeCount: null}
    
        },
        async get24hStats(){
            var netValues = await this.getThorNetValues()
            var runePrice = await this.getRunePrice()
            if(netValues){
                if(netValues.totalTx24h)
                var tx24h = utils.numberWithCommas(netValues.totalTx24h)
                else
                var tx24h = null
                if(netValues.users24h)
                var users24h = utils.numberWithCommas(netValues.users24h)
                else
                users24h = null

                if(runePrice){
                    var volume = parseInt(netValues.volume24h / 100000000)
                    volume = parseInt(volume * runePrice)
                    var volume24h = utils.numberWithCommas(volume)
                } else {
                    var volume24h = null
                }

                return {tx24h, users24h, volume24h}
            }
            return {tx24h: null, users24h: null, volume24h: null}
        },
        async getBondingIncome(){

            var minimumBond = await this.getMinimumBond()
            var netValues = await this.getThorNetValues()
            var runePrice = await this.getRunePrice()

            if(minimumBond && netValues && runePrice){

                var nodeIncomeYearly = netValues.bondingAPY * minimumBond
                var nodeIncomeYearlyUSD = (nodeIncomeYearly * runePrice)
                var nodeIncomeDaily = (nodeIncomeYearly / 365)
                var nodeIncomeDailyUSD = (nodeIncomeDaily * runePrice)
                
                nodeIncomeYearly = utils.numberWithCommas(parseInt(nodeIncomeYearly))
                if(nodeIncomeYearlyUSD > 1)
                nodeIncomeYearlyUSD = utils.numberWithCommas(parseInt(nodeIncomeYearlyUSD))
                else
                nodeIncomeYearlyUSD = nodeIncomeYearlyUSD.toFixed(2)

                if(nodeIncomeDaily > 1)
                nodeIncomeDaily = utils.numberWithCommas(parseInt(nodeIncomeDaily))
                else
                nodeIncomeDaily = nodeIncomeDaily.toFixed(2)

                if(nodeIncomeDailyUSD > 1)
                nodeIncomeDailyUSD = utils.numberWithCommas(parseInt(nodeIncomeDailyUSD))
                else
                nodeIncomeDailyUSD = nodeIncomeDailyUSD.toFixed(2)
                
                return {nodeIncomeDaily, nodeIncomeDailyUSD, nodeIncomeYearly, nodeIncomeYearlyUSD}
            }

            return {nodeIncomeDaily: null, nodeIncomeDailyUSD: null,
                 nodeIncomeYearly: null, nodeIncomeYearlyUSD: null}
        }
    })
}