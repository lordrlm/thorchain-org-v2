import utils from '../utils'

export const state = () => ({
    scrollY: 0,
    technicalPageData: null,
    coingeckoData: null,
    showProvideLiq: false,
    showYoutubeOverlay: false,
})

export const mutations = {
    scrollChange(state, payload) {
        state.scrollY = payload.amount
    },
    showProvide(state, payload) {
        state.showProvideLiq = payload
    },
    showYoutubeOverlay(state, payload){
        state.showYoutubeOverlay = payload
    }
}

export const getters = {
    scrollY: state => {
        return state.scrollY
    },
    showProvideLiq: state => {
        return state.showProvideLiq
    },
    showYoutubeOverlay: state =>{
        return state.showYoutubeOverlay
    }
}

export const actions = {
    async nuxtServerInit () {
        
    }   
}