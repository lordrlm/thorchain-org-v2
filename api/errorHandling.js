const telegramBot = require('./telegramBot.js')

module.exports = {
    reportError(msg, errorObj){
        // if(func){
        //     if(typeof func === "function")
        //         errorMsg += ("Error at " + getFunctionName(func) + "\n")
        //     else
        //         errorMsg += ("Error at "+ func +"\n")
        // }
        // else
        //     errorMsg += ("Error (function not specified)  \n")

        var errorMsg = "⚠️ " + msg + "\n"
        if(errorObj && typeof errorObj === "object")
            errorMsg += JSON.stringify(errorObj, null, 2)
        console.error(errorMsg)
        // telegramBot.log(errorMsg)
    }
}

function getFunctionName(func) 
{
   var name = func.toString();
   name = name.substr('function '.length);
   name = name.substr(0, name.indexOf('('));
    return name
}